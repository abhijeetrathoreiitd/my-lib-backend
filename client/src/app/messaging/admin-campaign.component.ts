import { Component, EventEmitter } from "@angular/core";
import { Api } from '../providers/api';
import { CurrentUser } from '../providers/currentUser';
import * as Materialize from "angular2-materialize/dist/index";
import * as _ from 'lodash';
@Component({
  selector: 'admin-campaign',
  templateUrl: "./admin-campaign.component.html"
})
export class AdminCampaignComponent {
  private sendMessageData: any = {
    message_title: '',
    message_content: ''
  }
  private _messages: any = [];
  private newMessage = false;
  private loading: boolean = false;
  constructor(private api: Api, private currentUser: CurrentUser, ) {
    this.getMessage()
  }

  submitMessage() {
    this.sendMessageData.user = this.currentUser.info.userId;
    this.api.post("messages", this.sendMessageData)
      .map(res => res.json())
      .subscribe((response) => {
        this.sendMessageData = {
          message_title: '',
          message_content: ''
        };
        this.getMessage()
        this.newMessage = false;
        this.loading = false;
      }, error => {
        this.loading = false;
      });
  }

  getMessage() {
    this.loading = true;
    this.api.get('messages?filter[where][delete]=false')
      .map(res => res.json())
      .subscribe((response) => {
        this._messages = response;
        this.loading = false;
      }, error => {
        this.loading = false;
      });
  }

  deleteMessage(message) {
    this.api.put('messages/' + message.id, {
      delete: true
    })
      .map(res => res.json())
      .subscribe((response) => {
        let index = _.findIndex(this._messages, ['id', message.id]);
        this.getMessage()
        this.loading = false;
      }, error => {
        this.loading = false;
      });
  }

  postMessage(message) {
    this.api.get('users?filter[where][realm]=user&filter[where][banned]=false')
      .map(res => res.json())
      .subscribe((response) => {
        var registredTokens = [];
        _.each(response, function (obj) {
          if (obj.firebaseDeviceToken)
            registredTokens.push(obj.firebaseDeviceToken)
        });
        if (registredTokens.length) {
          this.api.pushNotification(JSON.stringify({
            "getNotificationKeyObj": {
              "operation": "create",
              "notification_key_name": new Date().getTime().toString(),
              "registration_ids": registredTokens
            },
            "sendNotificationObject": {
              "to": '',
              "priority": "high",
              "notification": {
                "body": message.message_content,
                "title": message.message_title,
                "icon": "new"
              },
              "data": {
                "volume": "3.21.15",
                "contents": "http://www.news-magazine.com/world-week/21659772"
              }
            }
          }))
            .map(res => res.json())
            .subscribe((response) => {
              if(response.success)
                Materialize.toast(response.success, 5000);
              else 
                Materialize.toast(response.error, 5000);
              this.loading = false;
            }, error => {
              this.loading = false;
            });
          this.loading = false;
        } else {
          Materialize.toast(`No User able to get Push Notification`, 5000);
        }
      }, error => {
        this.loading = false;
      });
  }
}
