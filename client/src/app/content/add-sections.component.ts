import { Subscription } from "rxjs/Subscription";
import { Component, OnDestroy, OnInit, Inject, EventEmitter } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Api } from '../providers/api';
import { AngularFireDatabase } from 'angularfire2/database';
import * as firebase from 'firebase';
import * as _ from 'lodash'
import * as Materialize from "angular2-materialize/dist/index";
import { Common } from "../providers/common";
import { FirebaseApp } from 'angularfire2';
import { MaterializeAction } from "angular2-materialize/dist/index";
@Component({
  selector: 'add-section',
  styleUrls: ["./add-sections.component.css"],
  templateUrl: "./add-sections.component.html",
})

export class AddSectionsComponent implements OnDestroy, OnInit {
  public textEditorToolbarButtons = ['fullscreen', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '|', 'fontFamily', 'fontSize', 'color', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', '-', 'insertLink', 'insertTable', '|', 'specialCharacters', 'insertHR', 'selectAll', 'clearFormatting', '|', 'print', 'spellChecker', 'help', 'html', '|', 'undo', 'redo'];
  public topicId;
  public lesson_title;
  public lesson_icon;
  public lsections = [];
  private loading: boolean = false;
  private videoUpdate: boolean = true;
  paramsSubscription: Subscription;
  public storageRef = firebase.storage().ref();
  public froalaOptions: any = {
    placeholderText: 'Edit Your Content Here!',
    charCounterCount: false
  }
  modalActions = new EventEmitter<string | MaterializeAction>();
  private deleteSectionData: any;
  constructor(public router: Router, @Inject(FirebaseApp) firebaseApp: any,
    public route: ActivatedRoute,
    public api: Api,
    public common: Common,
    public af: AngularFireDatabase) {
    this.froalaOptions.toolbarButtons = this.textEditorToolbarButtons;
    this.froalaOptions.toolbarButtonsXS = this.textEditorToolbarButtons;
    this.froalaOptions.toolbarButtonsSM = this.textEditorToolbarButtons;
    this.froalaOptions.toolbarButtonsMD = this.textEditorToolbarButtons;
    this.storageRef = firebaseApp.storage().ref();
  }

  ngOnDestroy() {
    this.paramsSubscription.unsubscribe();
  }

  ngOnInit() {
    this.paramsSubscription = this.route.params.subscribe(params => this.topicId = params['topicId']);
    this.addNewSection();
  }

  saveSections() {
    this.loading = true;
    this.api.post("lessons", {
      "lesson_title": this.lesson_title,
      "lesson_icon": this.lesson_icon,
      "topicId": this.topicId
    })
      .map(res => res.json())
      .subscribe(
      res => {
        console.log("res", res);
        _.forEach(this.lsections, lsection => lsection.lessonId = res.id);
        this.api.post("lsections", this.lsections)
          .map(response => response.json())
          .subscribe(
          response => {
            this.loading = false;
            Materialize.toast(`Lesson added successfully`, 5000);
            this.router.navigate(['../../topics', this.topicId], { relativeTo: this.route });
          },
          error => {
            console.warn("lesson was created but unable to create lsections");
            this.loading = false;
            Materialize.toast(`Oops! Something bad happened, retry`, 5000);
          })
      },
      error => {
        this.loading = false;
        Materialize.toast(`Oops! Something bad happened, retry`, 5000);
      });
  }

  addNewSection() {
    if (!this.lsections)
      this.lsections = [];
    this.lsections.push({
      "mediaURI": "",
      "imageURI": "",
      "section_description": "",
      "archived": false
    })
  }

  lessonIconUpload(event) {
    this.loading = true;
    let file = event.srcElement.files[0];
    if (file && this.common.checkSupportedFormat(file, 'image')) {
      this.storageRef.child(`${'lessons/icons'}/${new Date().getTime() + file.name}`).put(file)
        .then((snapshot) => {
          this.loading = false;
          this.lesson_icon = snapshot.downloadURL;
          Materialize.toast(`Lesson Icon successfully uploaded`, 5000);
        })
        .catch(error => {
          this.loading = false;
          Materialize.toast(`Oops! Something bad happened, retry`, 5000);
        })
    }
    else {
      Materialize.toast(`Unsupported file or not an image`, 5000);
      this.loading = false;
    }
  }

  uploadMedia(event, i) {
    console.log("file ", event.srcElement.files[0]);
    this.loading = true;
    this.videoUpdate = false;
    let file = event.srcElement.files[0];
    if (file && this.common.checkSupportedFormat(file, 'video')) {
      this.storageRef.child(`${'lessons/videos'}/${new Date().getTime() + file.name}`).put(file)
        .then(snapshot => {
          console.log(snapshot.downloadURL)
          // this.loading = false;
          // this.videoUpdate = true;
          // this.lsections[i].mediaURI = snapshot.downloadURL;
          this.api.uploadAndEncryptVideo('encryptVideo', {
            "url": snapshot.downloadURL
          })
            .map(res => res.json())
            .subscribe(encryptVideoSuccess => {
              console.log('encryptVideoSuccess', encryptVideoSuccess);
              if (encryptVideoSuccess && encryptVideoSuccess.status === 200) {
                Materialize.toast(encryptVideoSuccess.success, 5000);
                this.api.uploadAndEncryptVideo('checkJobStatus', {
                  newlyCreatedJob: encryptVideoSuccess.data
                })
                  .map(res => res.json())
                  .subscribe(jobCreatedSuccess => {
                    console.log('jobCreatedSuccess', jobCreatedSuccess);
                    if (jobCreatedSuccess && jobCreatedSuccess.status === 200) {
                      this.loading = false;
                      this.videoUpdate = true;
                      this.lsections[i].mediaURI = jobCreatedSuccess.data.manifestUrls.mpdUrl;
                      this.lsections[i].optional = {
                        thumbnailUrl: jobCreatedSuccess.data.input.thumbnailUrl
                      };
                      Materialize.toast(jobCreatedSuccess.success, 5000);
                    } else {
                      this.loading = false;
                      Materialize.toast(`Getting Error On Upload Please After Some Time`, 5000);
                    }
                  }, jobCreatedError => {
                    this.loading = false;
                    console.log('uploadError', jobCreatedError);
                    if (!jobCreatedError.ok)
                      Materialize.toast(`Getting Error On Upload Please After Some Time`, 5000);
                  })
              } else {
                this.loading = false;
                Materialize.toast(`Getting Error On Upload Please After Some Time`, 5000);
              }
            }, encryptVideoError => {
              this.loading = false;
              console.log('uploadError', encryptVideoError);
              if (!encryptVideoError.ok)
                Materialize.toast(`Getting Error On Upload Please After Some Time`, 5000);
            });
          Materialize.toast(`Media successfully uploaded`, 5000);
        })
        .catch(error => {
          console.log("in catch", error);
          this.loading = false;
          this.videoUpdate = true;
          Materialize.toast(`Oops! Something bad happened, retry`, 5000);
        })
    }
    else {
      Materialize.toast(`Unsupported file or not a video`, 5000);
      this.loading = false;
    }
  }

  attachImage(event, i) {
    this.loading = true;
    let file = event.srcElement.files[0];
    if (file && this.common.checkSupportedFormat(file, 'image')) {
      this.storageRef.child(`${'lessons/pictures'}/${new Date().getTime() + file.name}`).put(file)
        .then(snapshot => {
          this.loading = false;
          this.lsections[i].imageURI = snapshot.downloadURL;
          Materialize.toast(`Image successfully uploaded`, 5000);
        })
        .catch(error => {
          this.loading = false;
          Materialize.toast(`Oops! Something bad happened, retry`, 5000);
        })
    }
    else {
      Materialize.toast(`Unsupported file or not a video`, 5000);
      this.loading = false;
    }
  }

  uploadZip(event, i) {
    this.loading = true;
    let file = event.srcElement.files[0];
    if (file) {
      this.storageRef.child(`${'lessons/videos'}/${file.name}`).put(file)
        .then(snapshot => {
          console.log(snapshot.downloadURL)
          this.loading = false;
          this.lsections[i].zipURI = snapshot.downloadURL;
          Materialize.toast(`Zip successfully uploaded`, 5000);
        })
        .catch(error => {
          this.loading = false;
          Materialize.toast(`Oops! Something bad happened, retry`, 5000);
        })
    }
    else {
      Materialize.toast(`Unsupported file or not a video`, 5000);
      this.loading = false;
    }
  }

  removeSection() {
    let section = this.deleteSectionData.section;
    let index = this.deleteSectionData.index;
    if (section.id) {
      this.api.patchdelete(`lsections/${section.id}`, { "archived": true })
        .map(response => response.json())
        .subscribe(
        response => {
          console.log(response)
          this.lsections.splice(index, 1);
          this.deleteSectionData = null;
          Materialize.toast(`Section Delete successfully`, 5000);
        }, error => {
          this.deleteSectionData = null;
          console.error("error in delete lsection");
        })
    } else {
      this.lsections.splice(index, 1);
      this.deleteSectionData = null;
      Materialize.toast(`Section Delete successfully`, 5000);
    }
  }
}
