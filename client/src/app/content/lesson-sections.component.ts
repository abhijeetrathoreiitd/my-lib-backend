import { Subscription } from "rxjs/Subscription";
import { Component, OnDestroy, OnInit, Inject, EventEmitter } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Api } from '../providers/api';
import * as firebase from 'firebase';
import * as _ from 'lodash'
import * as Materialize from "angular2-materialize/dist/index";
import { Common } from '../providers/common';
import { FirebaseApp } from 'angularfire2';
import { MaterializeAction } from "angular2-materialize/dist/index";
@Component({
  selector: 'lesson-section',
  styleUrls: ["./lesson-sections.component.css"],
  templateUrl: "./lesson-sections.component.html",
})

export class LessonSectionsComponent implements OnDestroy, OnInit {
  public lessonId;
  public lesson_title;
  public lesson_icon;
  public lsections = [];
  public topicId: string;
  private loading: boolean = false;
  private deleteSectionData: any;
  paramsSubscription: Subscription;
  modalActions = new EventEmitter<string | MaterializeAction>();
  public storageRef = firebase.storage().ref();
  public textEditorToolbarButtons = ['fullscreen', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '|', 'fontFamily', 'fontSize', 'color', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', '-', 'insertLink', 'insertTable', '|', 'specialCharacters', 'insertHR', 'selectAll', 'clearFormatting', '|', 'print', 'spellChecker', 'help', 'html', '|', 'undo', 'redo'];
  public froalaOptions: any = {
    placeholderText: 'Edit Your Content Here!',
    charCounterCount: false
  }
  constructor(public router: Router, @Inject(FirebaseApp) firebaseApp: any,
    public route: ActivatedRoute,
    public common: Common,
    public api: Api) {
    this.froalaOptions.toolbarButtons = this.textEditorToolbarButtons;
    this.froalaOptions.toolbarButtonsXS = this.textEditorToolbarButtons;
    this.froalaOptions.toolbarButtonsSM = this.textEditorToolbarButtons;
    this.froalaOptions.toolbarButtonsMD = this.textEditorToolbarButtons;
    this.storageRef = firebaseApp.storage().ref();
  }

  ngOnDestroy() {
    this.paramsSubscription.unsubscribe();
  }

  ngOnInit() {
    this.paramsSubscription = this.route.params.subscribe(params => this.lessonId = params['lessonId']);
    console.info("Received lessonId ", this.lessonId);
    this.fetchLesson();
    this.getSections();
  }

  saveSections(): void {
    this.loading = true;
    this.api.patch(`lessons/${this.lessonId}`, {
      "lesson_title": this.lesson_title,
      "lesson_icon": this.lesson_icon,
      "topicId": this.topicId
    })
      .map(res => res.json())
      .subscribe(
      res => {
        Materialize.toast(`Lesson updated successfully`, 5000);
        _.forEach(this.lsections, lsection => {
          let lid = lsection.id;
          delete lsection.id;
          lsection.lessonId = res.id;
          if (lid) {
            this.api.patch(`lsections/${lid}`, lsection)
              .map(response => response.json())
              .subscribe(
              response => {
                // Materialize.toast(`Lesson updated successfully`, 5000);
                this.router.navigate(['../../topics', this.topicId], { relativeTo: this.route });
              }, error => {
                console.error("error in saving lsection");
                // Materialize.toast(`Oops! Something bad happened, retry`, 5000);
              })
          }
          else {
            this.api.post(`lsections/`, lsection)
              .map(response => response.json())
              .subscribe(
              response => {
                // console.log("Success", response);
                this.router.navigate(['../../topics', this.topicId], { relativeTo: this.route });
              },
              error => {
                console.error("error in lsection saving");
                //_todo Handle error in async
              })
          }
        });
      },
      error => {
        Materialize.toast(`Oops! Something bad happened, retry`, 5000);
      });
  }

  addNewSection() {
    let idlength = this.lsections.length;
    this.lsections.push({
      "mediaURI": "",
      "imageURI": "",
      "section_description": ""
    })
  }

  fetchLesson() {
    this.loading = true;
    this.api.get(`lessons/${this.lessonId}`)
      .map(res => res.json())
      .subscribe(
      response => {
        this.loading = false;
        this.lesson_icon = response.lesson_icon;
        this.lesson_title = response.lesson_title;
        this.topicId = response.topicId
      },
      error => {
        this.loading = false;
        Materialize.toast(`Unable to fetch lesson`, 5000);
      }
      )
  }

  getSections() {
    this.loading = true;
    this.api.get(`lsections?filter={"where": {"lessonId": "${this.lessonId}", "archived": false}}`)
      .map(res => res.json())
      .subscribe(
      response => {
        this.loading = false;
        this.lsections = response;
      },
      error => {
        this.loading = false;
        Materialize.toast(`Unable to fetch lesson details`, 5000);
      }
      );
  }

  lessonIconUpload(event) {
    this.loading = true;
    let file = event.srcElement.files[0];
    if (file && this.common.checkSupportedFormat(file, 'image')) {
      this.storageRef.child(`${'lessons/icons'}/${new Date().getTime() + file.name}`).put(file)
        .then(snapshot => {
          this.loading = false;
          this.lesson_icon = snapshot.downloadURL;
          Materialize.toast(`Lesson Icon successfully uploaded`, 5000);
        })
        .catch(error => {
          this.loading = false;
          Materialize.toast(`Oops! Something bad happened, retry`, 5000);
        })
    }
    else {
      Materialize.toast(`Unsupported file or not an image`, 5000);
      this.loading = false;
    }
  }

  uploadMedia(event, i) {
    this.loading = true;
    let file = event.srcElement.files[0];
    if (file && this.common.checkSupportedFormat(file, 'video')) {
      this.storageRef.child(`${'lessons/videos'}/${new Date().getTime() + file.name}`).put(file)
        .then(snapshot => {
          console.log(snapshot.downloadURL)
          this.api.uploadAndEncryptVideo('encryptVideo', {
            "url": snapshot.downloadURL
          })
            .map(res => res.json())
            .subscribe(encryptVideoSuccess => {
              console.log('encryptVideoSuccess', encryptVideoSuccess);
              if (encryptVideoSuccess && encryptVideoSuccess.status === 200) {
                Materialize.toast(encryptVideoSuccess.success, 5000);
                this.api.uploadAndEncryptVideo('checkJobStatus', {
                  newlyCreatedJob: encryptVideoSuccess.data
                })
                  .map(res => res.json())
                  .subscribe(jobCreatedSuccess => {
                    console.log('jobCreatedSuccess', jobCreatedSuccess);
                    if (jobCreatedSuccess && jobCreatedSuccess.status === 200) {
                      Materialize.toast(jobCreatedSuccess.success, 5000);
                      this.loading = false;
                      this.lsections[i].mediaURI = jobCreatedSuccess.data.manifestUrls.mpdUrl;
                      this.lsections[i].optional = {
                        thumbnailUrl : jobCreatedSuccess.data.input.thumbnailUrl
                      };
                    } else {
                      this.loading = false;
                      Materialize.toast(`Getting Error On Upload Please After Some Time`, 5000);
                    }
                  }, jobCreatedError => {
                    this.loading = false;
                    console.log('uploadError', jobCreatedError);
                    if (!jobCreatedError.ok)
                      Materialize.toast(`Getting Error On Upload Please After Some Time`, 5000);
                  })
              } else {
                this.loading = false;
                Materialize.toast(`Getting Error On Upload Please After Some Time`, 5000);
              }
            }, encryptVideoError => {
              this.loading = false;
              console.log('uploadError', encryptVideoError);
              if (!encryptVideoError.ok)
                Materialize.toast(`Getting Error On Upload Please After Some Time`, 5000);
            });
          Materialize.toast(`Media successfully uploaded`, 5000);
        })
        .catch(error => {
          this.loading = false;
          Materialize.toast(`Oops! Something bad happened, retry`, 5000);
        })
    }
    else {
      Materialize.toast(`Unsupported file or not a video`, 5000);
      this.loading = false;
    }
  }

  attachImage(event, i) {
    this.loading = true;
    let file = event.srcElement.files[0];
    if (file && this.common.checkSupportedFormat(file, 'image')) {
      this.storageRef.child(`${'lessons/pictures'}/${new Date().getTime() + file.name}`).put(file)
        .then(snapshot => {
          this.loading = false;
          Materialize.toast(`Image successfully uploaded`, 5000);
          this.lsections[i].imageURI = snapshot.downloadURL;
        })
        .catch(error => {
          this.loading = false;
          Materialize.toast(`Oops! Something bad happened, retry`, 5000);
        })
    }
    else {
      Materialize.toast(`Unsupported file or not an image`, 5000);
      this.loading = false;
    }
  }
  uploadZip(event, i) {
    this.loading = true;
    let file = event.srcElement.files[0];
    if (file) {
      this.storageRef.child(`${'lessons/videos'}/${file.name}`).put(file)
        .then(snapshot => {
          console.log(snapshot.downloadURL)
          this.loading = false;
          this.lsections[i].zipURI = snapshot.downloadURL;
          Materialize.toast(`Zip successfully uploaded`, 5000);
        })
        .catch(error => {
          this.loading = false;
          Materialize.toast(`Oops! Something bad happened, retry`, 5000);
        })
    }
    else {
      Materialize.toast(`Unsupported file or not a video`, 5000);
      this.loading = false;
    }
  }

  removeSection() {
    let section = this.deleteSectionData.section;
    let index = this.deleteSectionData.index;
    if (section.id) {
      this.api.patchdelete(`lsections/${section.id}`, { "archived": true })
        .map(response => response.json())
        .subscribe(
        response => {
          console.log(response)
          this.lsections.splice(index, 1);
          this.deleteSectionData = null;
          Materialize.toast(`Section Delete successfully`, 5000);
        }, error => {
          this.deleteSectionData = null;
          console.error("error in delete lsection");
        })
    } else {
      this.lsections.splice(index, 1);
      this.deleteSectionData = null;
      Materialize.toast(`Section Delete successfully`, 5000);
    }
  }
}
