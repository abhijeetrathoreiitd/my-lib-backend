import { Component, EventEmitter } from "@angular/core";
import { MaterializeAction } from "angular2-materialize/dist/index";
import { Router, ActivatedRoute } from "@angular/router";
import { Api } from '../providers/api';
import * as Materialize from "angular2-materialize/dist/index";

@Component({
  selector: 'admin-subjects',
  styleUrls: ['./admin-subjects.component.css'],
  templateUrl: "./admin-subjects.component.html",
})

export class AdminSubjectsComponent {
  private subjects = [];
  public newSubject = {
    subject_name: '',
    subject_color: '',
  };
  public editSubject = {
    subject_name: '',
    subject_color: '',
    id: ''
  };
  public loading: boolean = false;
  modalActionsDelete = new EventEmitter<string | MaterializeAction>();
  private deleteData: any;
  constructor(private router: Router,
    private route: ActivatedRoute,
    private api: Api) {
    this.getSubjects();
  }

  onSelect(subject: any): void {
    this.router.navigate(['../subjects', subject.id], { relativeTo: this.route });
  }

  addSubject(form): void {
    this.loading = true;
    this.api.post('subjects', this.newSubject)
      .map(res => res.json())
      .subscribe((response) => {
        this.loading = false;
        Materialize.toast(`${this.newSubject.subject_name} created successfully`, 5000);
        this.getSubjects();
        this.closeModal(form);
      }, error => {
        this.loading = false;
        Materialize.toast(`Oops! Something bad happened, retry`, 5000);
      });
  }

  getSubjects(): void {
    this.loading = true;
    this.api.get('subjects?filter[where][archived]=false')
      .map(res => res.json())
      .subscribe((response) => {
        this.loading = false;
        this.subjects = response;
      }, error => {
        this.loading = false;
        Materialize.toast(`Oops! Something bad happened, retry`, 5000);
      });
  }

  updateSubject(form): void {
    this.loading = true;
    this.api.patch('subjects/' + this.editSubject.id, this.editSubject)
      .map(res => res.json())
      .subscribe((response) => {
        this.loading = false;
        Materialize.toast(`${this.editSubject.subject_name} updated successfully`, 5000);
        this.getSubjects();
        this.closeModalEdit(form);
      }, error => {
        this.loading = false;
        Materialize.toast(`Oops! Something bad happened, retry`, 5000);
      });
  }

  deleteSubject(subject): void {
    console.warn("sub", subject.id);
    this.loading = true;
    this.api.patchdelete('subjects/' + subject.id, {"archived": true})
      .map(res => res.json())
      .subscribe((response) => {
        this.loading = false;
        this.getSubjects();
        Materialize.toast(`${subject.subject_name} deleted successfully`, 5000);
      }, error => {
        this.loading = false;
        Materialize.toast(`Oops! Something bad happened, retry`, 5000);
      });
  }

  modalActionsAdd = new EventEmitter<string | MaterializeAction>();
  modalActionsEdit = new EventEmitter<string | MaterializeAction>();

  openModal() {
    this.modalActionsAdd.emit({ action: "modal", params: ['open'] });
  }

  closeModal(form) {
    form.resetForm();
    this.modalActionsAdd.emit({ action: "modal", params: ['close'] });
  }

  openModalEdit(subject): void {
    this.editSubject = Object.assign({}, subject);
    this.modalActionsEdit.emit({ action: "modal", params: ['open'] });
  }

  closeModalEdit(form) {
    form.resetForm();
    this.modalActionsEdit.emit({ action: "modal", params: ['close'] });
  }
}
