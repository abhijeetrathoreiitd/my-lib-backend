import { Injectable } from '@angular/core';
import { Http, RequestOptions, URLSearchParams, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class Api {
  url: string = "https://mylibapp.com/api";  //prod
  directurl: string = "https://mylibapp.com/"; // without API Tag
  public logoutTimeout = 60000;
  // url: string = "http://104.198.106.68:8080/api";   //dev
  // url: string = "http://0.0.0.0:8080/api";  //local
  private options: any;
  constructor(public http: Http, private router: Router) {
  }

  get(endpoint: string, params?: any, options?: RequestOptions) {
    if (this.updateUserExpireTime()) {
      if (!options) {
        options = new RequestOptions();
      }

      // Support easy query params for GET requests
      if (params) {
        let p = new URLSearchParams();
        for (let k in params) {
          p.set(k, params[k]);
        }
        // Set the search field if we have params and don't already have
        options.search = !options.search && p || options.search;
      }
      return this.http.get(this.url + '/' + endpoint, options);
    }
  }

  post(endpoint: string, body: any, options?: RequestOptions) {
    if (this.updateUserExpireTime()) {
      return this.http.post(this.url + '/' + endpoint, body, options)
    }
  }

  put(endpoint: string, body: any, options?: RequestOptions) {
    if (this.updateUserExpireTime()) {
      return this.http.put(this.url + '/' + endpoint, body, options);
    }
  }

  delete(endpoint: string, options?: RequestOptions) {
    if (this.updateUserExpireTime()) {
      return this.http.delete(this.url + '/' + endpoint, options);
    }
  }

  patch(endpoint: string, body: any, options?: RequestOptions) {
    if (this.updateUserExpireTime()) {
      return this.http.put(this.url + '/' + endpoint, body, options);
    }
  }

  patchdelete(endpoint: string, body: any, options?: RequestOptions) {
    if (this.updateUserExpireTime()) {
      return this.http.patch(this.url + '/' + endpoint, body, options);
    }
  }

  pushNotification(body: any) {
    if (this.updateUserExpireTime()) {
      return this.http.get(this.directurl + 'myLibPushNotification?notification=' + body);
    }
  }

  uploadAndEncryptVideo(endpoint: string, body: any, options?: RequestOptions) {
    if (this.updateUserExpireTime()) {
      return this.http.post(this.directurl + endpoint, body, options);
    }
  }

  getJobs(endpoint: string) {
    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded', 'bitcodin-api-version': 'v1', 'bitcodin-api-key': 'db2fcf3470fb97178c2bf51368d60ec5c3e0d0eb69b2ea9e52e5c065e4e55631', 'Access-Control-Allow-Origin': '*' });
    let options = new RequestOptions({ headers: headers });
    if (this.updateUserExpireTime()) {
      return this.http.get('https://private-anon-2f1919375a-bitcodinrestapi.apiary-proxy.com/api/job/' + endpoint, options);
    }
  }

  updateUserExpireTime() {
    let userToken: any = localStorage.getItem('currentUserToken') || null;
    userToken = JSON.parse(userToken);
    if (userToken && userToken.expireTime && new Date().getTime() < new Date(userToken.expireTime).getTime()) {
      var now = new Date();
      now.setHours(now.getHours() + 1);
      console.log("now", now)
      userToken.expireTime = now;
      localStorage.setItem('currentUserToken', JSON.stringify(userToken));
      
    }
    return true;
  }
}
