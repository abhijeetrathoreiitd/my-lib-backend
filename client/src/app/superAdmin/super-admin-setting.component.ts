import {Component} from "@angular/core";
import {LoginService} from "../providers/login.service"
import {NgForm} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {CurrentUser} from '../providers/currentUser';
import {Api} from '../providers/api';
import * as Materialize from "angular2-materialize/dist/index";

@Component({
  selector: 'super-admin-setting',
  templateUrl: './super-admin-setting.component.html',
  styles: [`
    .save-button{
  position: fixed;
  bottom: 0rem;
  left: 11rem;
  padding-top: 10px;
  padding-bottom: 10px;
  width: 100%;
  background-color: #9e9e9e;
}
  `]

})
export class SuperAdminSettingComponent {

  profile = this.currentUser.info || {};
  password2 = '';
  loading = false;
  returnUrl:string;

  constructor(public loginService:LoginService,
              public currentUser:CurrentUser,
              public router:Router,
              public route:ActivatedRoute,
              public api:Api) {
  }

  onSubmit(form:NgForm) {
    let details = Object.assign({}, this.profile);
    this.loading = true;
    this.api.put("users/updateUser", {details: details})
      .map(res => res.json())
      .subscribe(
        response => {
          // console.info("response", response);
          this.loading = false;
          Materialize.toast(`Successfully updated profile`, 5000);
          this.router.navigate(['../../super/home'], {relativeTo: this.route});
        },
        error => {
          Materialize.toast(`Oops! Something bad happened, retry`, 5000);
          // console.log(error)
          this.loading = false;
        }
      );
  }
}
