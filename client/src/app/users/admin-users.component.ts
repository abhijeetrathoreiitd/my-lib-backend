import { Component, EventEmitter } from "@angular/core";
import { Api } from '../providers/api';
import * as _ from 'lodash';
import * as Materialize from "angular2-materialize/dist/index";
import { MaterializeAction } from "angular2-materialize/dist/index";
@Component({
  selector: 'admin-users',
  templateUrl: './admin-users.component.html',
})

export class AdminUsersComponent {
  public users = [];
  public loading: boolean = false;
  modalActionsDelete = new EventEmitter<string | MaterializeAction>();
  private deleteData: any;
  constructor(private api: Api) {
    this.getUsers();
  }

  getUsers() {
    this.loading = true;
    this.api.get(`users`)
      .map(res => res.json())
      .subscribe(res => {
        this.users = _.filter(res, user => {
          if (user.realm != 'admin' && user.realm != 'superuser') {
            if (user.subscribed_plan)
              user.subscribed_plan.subscriptionDate = new Date(user.subscribed_plan.subscriptionDate);
            return user
          }
          this.loading = false;
        },
          error => {
            this.loading = false;
            Materialize.toast(`Oops! Something bad happened, retry`, 5000);
          });
      });
  }

  deleteUser(user): void {
    this.loading = true;
    this.api.delete('users/' + user.userId)
      .map(res => res.json())
      .subscribe(
      response => {
        if (response.count == 1) {
          this.getUsers();
          Materialize.toast(`${user.full_name} deleted successfully`, 5000);
        }
        else {
          Materialize.toast(`Oops! Something bad happened, retry`, 5000);
        }
        this.loading = false;
      },
      error => {
        this.loading = false;
        Materialize.toast(`Oops! Something bad happened, retry`, 5000);
      });
  }

  banUser(user): void {
    this.loading = true;
    let details = Object.assign({}, user);
    this.api.put("users/updateUser", { details: details })
      .map(res => res.json())
      .subscribe(
      response => {
        this.loading = false;
        Materialize.toast(`${user.full_name} ${user.banned ? 'banned' : 'unbanned'} to use the app`, 5000);
      },
      error => {
        user.banned = !user.banned;
        this.loading = false;
        Materialize.toast(`Oops! Something bad happened, retry`, 5000);
      }
      );
  }
}
