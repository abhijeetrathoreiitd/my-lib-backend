import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AdminSideMenuComponent } from "../sideMenu/admin-side-menu.component";
import { AdminUsersComponent } from "../users/admin-users.component";
import { AdminSubjectsComponent } from "../content/admin-subjects.component";
import { AdminSubscriptionComponent } from "../subscription/admin-subscription.component";
import { AdminCampaignComponent } from "../messaging/admin-campaign.component";
import { AdminSettingsComponent } from "../settings/admin-settings.component";
import { SubjectBooksComponent } from "../content/subject-books.component";
import { BookTopicsComponent } from "../content/book-topics.component";
import { TopicLessonsComponent } from "../content/topic-lessons.component";
import { LessonSectionsComponent } from "../content/lesson-sections.component";
import { AddSectionsComponent } from "../content/add-sections.component";
import { LoggedInGuard } from "../guards/loggedin.guard";
import { PrivacyPolicyComponent } from '../privacy-policy/privacy-policy.component';
const adminRoutes: Routes = [{
  path: 'admin', component: AdminSideMenuComponent,
  children: [
    { path: 'users', component: AdminUsersComponent, canActivate: [LoggedInGuard] },
    { path: 'subjects', component: AdminSubjectsComponent, canActivate: [LoggedInGuard] },
    { path: 'subscriptions', component: AdminSubscriptionComponent, canActivate: [LoggedInGuard] },
    { path: 'campaign', component: AdminCampaignComponent, canActivate: [LoggedInGuard] },
    { path: 'settings', component: AdminSettingsComponent, canActivate: [LoggedInGuard] },
    { path: 'subjects/:subjectId', component: SubjectBooksComponent, canActivate: [LoggedInGuard] },
    { path: 'books/:bookId', component: BookTopicsComponent, canActivate: [LoggedInGuard] },
    { path: 'topics/:topicId', component: TopicLessonsComponent, canActivate: [LoggedInGuard] },
    { path: 'lessons/:lessonId', component: LessonSectionsComponent, canActivate: [LoggedInGuard] },
    { path: 'lesson/:topicId', component: AddSectionsComponent, canActivate: [LoggedInGuard] },
    { path: '', redirectTo: '/admin/subjects', pathMatch: 'full' },
    { path: '**', redirectTo: '/admin/subjects' }

  ]
},{ path: 'privacy-policy', component: PrivacyPolicyComponent }];

@NgModule({
  imports: [
    RouterModule.forChild(adminRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AdminRoutingModule {
}
