import { MyLibCliPage } from './app.po';

describe('my-lib-cli App', () => {
  let page: MyLibCliPage;

  beforeEach(() => {
    page = new MyLibCliPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
