'use strict';
var config = require('../../server/config.json');
var path = require('path');

module.exports = function (User) {
  //send verification email after registration
  User.afterRemote('create', function (context, user, next) {
    //   user.userId= user.id.toString();
    console.log('> user.afterRemote triggered', user);
    var options = {
      type: 'email',
      host: config.prod_ip,
      port: 8080,
      to: user.email,
      from: 'noreply@mylib.com',
      subject: 'Thanks for registering.',
      // text: 'my text',
      // html: 'my <em>html</em>',
      template: path.resolve(__dirname, '../../server/views/verify.ejs'),
      redirect: '/verified',
      user: user
    };

    user.verify(options, function (err, response) {
      if (err) {
        User.deleteById(user.id);
        return next(err);
      }

      console.log('> verification email sent:', response);

      // context.res.render('response', {
      //     title: 'Signed up successfully',
      //     content: 'Please check your email and click on the verification link ' +
      //     'before logging in.',
      //     redirectTo: '/',
      //     redirectToLinkText: 'Log in'
      // });
      
      context.res.send({
        'info': 'SignUp Successfuly',
        'status': 'success',
        'user': user
      })
    });
  });

  //send password reset link when requested
  User.on('resetPasswordRequest', function (info) {
    var url = config.resetPassword + '/reset-password';
    var html = 'Click <a href="' + url + '?access_token=' +
      info.accessToken.id + '">here</a> to reset your password <br> <img src="'+ config.resetPassword +'/assets/images/invite.png">';
    User.app.models.Email.send({
      to: info.email,
      from: info.email,
      subject: 'Password reset',
      html: html
    }, function (err) {
      if (err) return console.log('> error sending password reset email');
      console.log('> sending password reset email to:', info.email);
    });
  });

  User.updateUser = function (details, cb) {
    var newErrMsg, newErr;
    if (!details.userId) {
      newErrMsg = "UserId is missing!";
      newErr = new Error(newErrMsg);
      newErr.statusCode = 404;
      newErr.code = 'USER_ID_MISSING';
      cb(newErr, false);
    }
    else {
      try {
        this.findOne({where: {'userId': details.userId}}, function (err, user) {
          if (err) {
            cb(err, false);
          } else if (!user) {
            newErrMsg = "User account does not exists!";
            newErr = new Error(newErrMsg);
            newErr.statusCode = 401;
            newErr.code = 'USER_NOT_FOUND';
            cb(newErr);
          } else {
            console.info("user is", user);

            //_todo user details already exist check
            user.updateAttributes(details, function (err, instance) {
              if (err) {
                cb(err);
              } else {
                cb(null, true);
              }
            });
          }
        })
      } catch (err) {
        cb(err);
      }
    }
  }

  User.remoteMethod(
    'updateUser',
    {
      description: "Allows anyone to change a user details.",
      http: {verb: 'put'},
      accepts: [
        {arg: 'details', type: 'object', required: true, description: "User details to update"}
      ],
      returns: {arg: 'accountUpdated', type: 'boolean'}
    }
  )
};
