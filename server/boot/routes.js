var dsConfig = require('../datasources.json');
var config = require('../config.json');
const keyPublishable = config.keyPublishable;
const keySecret = config.keySecret;
const stripe = require("stripe")(keySecret);
var request = require('request');
const keyFilename = "./google-services.json"; //replace this with api key file
const projectId = "my-lib-project"; //replace with your project id
const bucketName = `${projectId}.appspot.com`;
var mime = require('mime');
var fs = require('fs');
var Q = require('q');
var bitcodin = require('bitcodin')('db2fcf3470fb97178c2bf51368d60ec5c3e0d0eb69b2ea9e52e5c065e4e55631');
const gcs = require('@google-cloud/storage')({
  projectId: 'my-lib-project',
  keyFilename: keyFilename
});
// gcs
//   .createBucket(bucketName)
//   .then(() => {
//     console.log(`Bucket ${bucketName} created.`);
//   })
//   .catch(err => {
//     console.error('ERROR:', err);
//   });
const bucket = gcs.bucket(bucketName);
var zipFolder = require('zip-folder');
module.exports = function (app) {
  var User = app.models.user;
  var Subscription = app.models.subscription
  //login page
  // app.get('/login', function(req, res) {
  //   var credentials = dsConfig.emailDs.transports[0].auth;
  //   res.render('login', {
  //     email: credentials.user,
  //     password: credentials.pass
  //   });
  // });

  //verified
  app.get('/verified', function (req, res) {
    res.render('verified');
  });
  //send an email with instructions to reset an existing user's password
  app.post('/request-password-reset', function (req, res, next) {
    User.resetPassword({
      email: req.body.email
    }, function (err) {
      if (err) return res.status(401).send(err);

      res.render('response', {
        title: 'Password reset requested',
        content: 'Check your email for further instructions',
        redirectTo: '/',
        redirectToLinkText: 'Log in'
      });
    });
  });

  // show password reset form
  app.get('/reset-password', function (req, res, next) {
    if (!req.accessToken) return res.sendStatus(401);
    res.render('password-reset', {
      accessToken: req.accessToken.id
    });
  });

  //reset the user's pasword
  app.post('/reset-password', function (req, res, next) {
    if (!req.accessToken) return res.sendStatus(401);

    //verify passwords match
    if (!req.body.password || !req.body.confirmation ||
      req.body.password !== req.body.confirmation) {
      return res.sendStatus(400, new Error('Passwords do not match'));
    }

    User.findById(req.accessToken.userId, function (err, user) {
      if (err) return res.sendStatus(404);
      user.updateAttribute('password', req.body.password, function (err, user) {
        if (err) return res.sendStatus(404);
        console.log('> password reset processed successfully');
        res.render('response', {
          title: 'Password reset success',
          content: 'Your password has been reset successfully',
          redirectTo: '/',
          redirectToLinkText: 'Log in'
        });
      });
    });
  });

  app.post("/charge", (req, res) => {
    var newErr;

    let amount = req.body.amount * 100;
    console.log('req.body', req.body)
    stripe.customers.create({
      email: req.body.email
    })
      .then(customer => {
        stripe.customers.createSource(
          customer.id,
          { source: req.body.stripeToken },
          (err => res.send({ "status": "failed", "error": err }), card => {
            stripe.charges.create({
              amount,
              description: req.body.description || "No description provided",
              currency: req.body.currency || 'ugx',
              customer: customer.id
            })
              .catch(err => {
                res.send({ "status": "failed", "error": err })
              })
              .then(charge => {
                if (charge)
                  res.send({ "status": charge.status, "charge": charge })
              });
          })
        )
      })
  });

  app.get('/myLibPushNotification', function (req, res) {
    //console.log(req.query.notification)
    var notificationData = JSON.parse(req.query.notification);
    request({
      uri: config.firePushNotificationConfig.getNotificationKeyUrl,
      method: "POST",
      timeout: 10000,
      followRedirect: true,
      maxRedirects: 10,
      json: notificationData.getNotificationKeyObj,
      headers: {
        'Authorization': config.firePushNotificationConfig.Authorization,
        'content-type': 'application/json',
        'project_id': config.firePushNotificationConfig.project_id,
      }
    }, function (error, response, notificationResult) {
      if (notificationResult) {
        //console.log(notificationResult)
        if (notificationResult.notification_key) {
          notificationData.sendNotificationObject.to = notificationResult.notification_key;
          request({
            uri: config.firePushNotificationConfig.sendPushNotificationUrl,
            method: "POST",
            timeout: 10000,
            followRedirect: true,
            maxRedirects: 10,
            json: notificationData.sendNotificationObject,
            headers: {
              'Authorization': config.firePushNotificationConfig.Authorization,
              'content-type': 'application/json'
            }
          }, function (error, response, body) {
            console.log(error, body)
            if (body && !body.error) {
              res.json({ success: "Send Notification Successfully", status: 200, data: response });
            } else {
              res.json({ error: "Error In Server", status: 400 });
            }
          });
        } else {
          res.json({ error: notificationResult.error, status: 400 });
        }
      } else {
        res.json({ error: "Error In Server", status: 400 });
      }
    });
  })

  app.get('/myLibPaymentCallback', function (req, res) {
    console.log('req', req);
    res.json({ error: "Success Payment", status: 200 });
  });

  app.post('/myLibPaymentCallback', function (req, res) {
    var successPaymentData = req.body;
    console.log('Calliing', successPaymentData.data.status)
    if (successPaymentData && Object.keys(successPaymentData).length !== 0 && successPaymentData.data.status === 'successful' && successPaymentData.data.collection_request && successPaymentData.data.collection_request.metadata) {
      var requestData = successPaymentData.data.collection_request.metadata;
      console.log('0', requestData)
      User.findById(requestData.userId, function (userDataError, userDataSuccess) {
        console.log('1', userDataError, userDataSuccess)
        if (userDataSuccess) {
          Subscription.findById(requestData.subId, function (subscriptionError, subscriptionSuccess) {
            console.log('2', subscriptionError, subscriptionSuccess)
            if (subscriptionSuccess) {
              var obj = subscriptionSuccess;
              obj.subscriptionDate = new Date();
              obj.deviceObject = requestData.device;
              userDataSuccess.updateAttributes({
                "subscribed_plan": obj
              }, function (errorData, successData) {
                console.log(errorData, successData);
                if (successData) {
                  request({
                    uri: config.firePushNotificationConfig.sendPushNotificationUrl,
                    method: "POST",
                    timeout: 10000,
                    followRedirect: true,
                    maxRedirects: 10,
                    json: {
                      "to": requestData.ftoken,
                      "priority": "high",
                      "notification": {
                        "body": "Payment Done",
                        "title": 'Payment Successfully',
                        "icon": "new"
                      },
                      "data": {
                        "volume": "3.21.15",
                        "contents": { "state": "payment", "data": successData }
                      }
                    },
                    headers: {
                      'Authorization': config.firePushNotificationConfig.Authorization,
                      'content-type': 'application/json'
                    }
                  }, function (error, response, body) {
                    if (body && !body.error) {
                      res.json({ success: "Send Notification Successfully", status: 200 });
                    } else {
                      res.json({ error: "Error In Server", status: 400 });
                    }
                  });
                  // request({
                  //   uri: config.firePushNotificationConfig.getNotificationKeyUrl,
                  //   method: "POST",
                  //   timeout: 10000,
                  //   followRedirect: true,
                  //   maxRedirects: 10,
                  //   json: {
                  //     "operation": "create",
                  //     "notification_key_name": new Date().getTime().toString(),
                  //     "registration_ids": [requestData.ftoken, userDataSuccess.firebaseDeviceToken]
                  //   },
                  //   headers: {
                  //     'Authorization': config.firePushNotificationConfig.Authorization,
                  //     'content-type': 'application/json',
                  //     'project_id': config.firePushNotificationConfig.project_id,
                  //   }
                  // }, function (error, response, notificationResult) {
                  //   if (notificationResult) {

                  //   } else {
                  //     res.json({ error: "Error In Server", status: 400 });
                  //   }
                  // });
                }
              });
            }
          });
        }
      });
    }
  });
  app.post('/encryptVideo', function (req, res) {
    var videoInfo = req.body;
    console.log('Calliing', videoInfo);
    var openMovieUrl = videoInfo.url,
      createInputPromise, createEncodingProfilePromise;
    // Create bitcodin Input
    createInputPromise = bitcodin.input.create(openMovieUrl);
    // Create bitcodin encoding profile. The ApiAry documentation which explains how such a
    // encoding profile should look like can be found at the link below
    // http://docs.bitcodinrestapi.apiary.io/#reference/encoding-profiles/create-an-encoding-profile
    var encodingProfileConfiguration = {
      "name": "bitcodin Encoding Profile",
      "videoStreamConfigs": [
        {
          "defaultStreamId": 0,
          "bitrate": 1200000,
          "profile": "Main",
          "preset": "premium",
          "height": 720,
          "width": 1280
        }
      ],
      "audioStreamConfigs": [
        {
          "defaultStreamId": 0,
          "bitrate": 128000
        }
      ]
    };
    var jobConfiguration = {
      "inputId": -1,
      "encodingProfileId": -1,//
      "outputId": 335119,// output id of Google cloud.
      "manifestTypes": ["mpd"],
      "drmConfig": {
        "system": "clearkey",
        "kid": "eb676abbcb345e96bbcf616630f1a3da",
        "key": "100b6c20940f779a4589152b57d2dacb",
        "method": "mpeg_cenc"
      }
    };
    //createEncodingProfilePromise = bitcodin.encodingProfile.create(encodingProfileConfiguration);
    // Create a bitcodin job which transcodes the video to DASH and HLS. The ApiAry documentation which explains
    // how a job configuration object should look like can be found at the following link below
    // http://docs.bitcodinrestapi.apiary.io/#reference/jobs/job/create-a-job
    Q.all([createInputPromise]).then( //createEncodingProfilePromise
      function (result) {
        console.log('Successfully created input', result);
        // console.log('result[1].encodingProfileId', result[1].encodingProfileId)
        jobConfiguration.inputId = result[0].inputId;
        jobConfiguration.encodingProfileId = 228416;
        bitcodin.job.create(jobConfiguration)
          .then(
          function (newlyCreatedJob) {
            console.log('Successfully created a new transcoding job:', newlyCreatedJob);
            //checkJobStatus(res, newlyCreatedJob);
            res.json({
              success: "Job Created Successfully",
              status: 200,
              data: newlyCreatedJob
            });
          },
          function (error) {
            console.log('Error while creating a new transcoding job:', error);
            res.json({ error: "Error In Server", status: 400 });
          }
          );
      },
      function (error) {
        console.log('Error while creating input and/or encoding profile:', error);
        res.json({ error: "Error In Server========", status: 500 });
      }
    );
  });

  app.post('/checkJobStatus', function (req, res) {
    checkJobStatus(res, req.body.newlyCreatedJob);
  });

  app.post('/checkTransferStatus', function (req, res) {
    _checkTransferStatus(res, req.body.newlyCreatedJob);
  })

  app.post('/getFilesFromBucket', function (req, res) {
    var newlyCreatedJob = req.body.newlyCreatedJob;
    var dir = './server/files/' + newlyCreatedJob.jobFolder;
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }
    bucket.getFiles({ prefix: 'videos/' + newlyCreatedJob.jobFolder + '/' },
      function (err, files) {
        console.log(err, files.length)
        var count = 0;
        files.forEach(function (file) {
          if (file.name) {
            var filePath = file.name.split('/');
            var folderCompleteName = dir;
            filePath.forEach(function (folderName, folderKey) {
              if (folderKey > 1 && folderKey < filePath.length - 1) {

                folderCompleteName = folderCompleteName + '/' + folderName;
              }
              if (folderKey === filePath.length)
                folderCompleteName = dir;
              if (!fs.existsSync(folderCompleteName)) {
                fs.mkdirSync(folderCompleteName);
              }
            })
            file.download({
              destination: './server/files/' + newlyCreatedJob.jobFolder + '/' + file.name.split('/').splice(2, file.name.split('/').length - 2).join('/')
            }, function (err) {
              count++;
              console.log('Download', err, files.length, count);
              if (files.length === count) {
                zipFolder('./server/files/' + newlyCreatedJob.jobFolder, './server/files/' + newlyCreatedJob.jobFolder + '.zip', function (err) {
                  if (err) {
                    console.log('oh no!', err);
                  } else {
                    res.json({
                      success: "Zip Done Successfully",
                      status: 200,
                      data: newlyCreatedJob
                    });
                  }
                });
              }
            })
          }
        })
      })
  })

  app.post('uploadZipFile', function (req, res) {
    var newlyCreatedJob = req.body.newlyCreatedJob
    var fileMime = mime.lookup('./server/files/' + newlyCreatedJob.jobFolder + '.zip');
    bucket.upload('./server/files/' + newlyCreatedJob.jobFolder + '.zip', {
      destination: 'videos/' + newlyCreatedJob.jobFolder + '.zip',
      public: true,
      metadata: { contentType: fileMime, cacheControl: "public, max-age=300" }
    }, function (err, file) {
      if (err) {
        console.log(err);
        return;
      }
      res.json({
        success: "Video encrypted Successfully", status: 200, data: {
          zipUrl: `http://storage.googleapis.com/${bucketName}/${encodeURIComponent('videos/' + newlyCreatedJob.jobFolder + '.zip')}`,
          mpdUrl: newlyCreatedJob.manifestUrls.mpdUrl
        }
      });
    });
  });

  function checkJobStatus(res, newlyCreatedJob) {
    bitcodin.job.getStatus(newlyCreatedJob.jobId).then(function (result) {
      console.log('checkJobStatus.status', result.status);
      if (result.status === 'Finished') {
        res.json({
          success: "Job Done Successfully",
          status: 200,
          data: newlyCreatedJob
        });
        _checkTransferStatus(newlyCreatedJob);
      } else {
        checkJobStatus(res, newlyCreatedJob);
      }
    }, function (err) {
      console.log(err);
    });
  }

  function _checkTransferStatus(newlyCreatedJob) {
    console.log(newlyCreatedJob.jobId)
    bitcodin.job.transfer.list(newlyCreatedJob.jobId).then(function (transferResult) {
      console.log(transferResult[0].status, transferResult[0].outputProfile)
      if (transferResult[0].status === 'finished') {
        console.log({
          success: "Transfer Done Successfully",
          status: 200,
          data: newlyCreatedJob
        });
      } else {
        _checkTransferStatus(newlyCreatedJob);
      }
    });
  }

  // Copyright 2017, Google, Inc.
  // Licensed under the Apache License, Version 2.0 (the "License");
  // you may not use this file except in compliance with the License.
  // You may obtain a copy of the License at
  //
  //    http://www.apache.org/licenses/LICENSE-2.0
  //
  // Unless required by applicable law or agreed to in writing, software
  // distributed under the License is distributed on an "AS IS" BASIS,
  // WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  // See the License for the specific language governing permissions and
  // limitations under the License.


  // Returns the public, anonymously accessable URL to a given Cloud Storage
  // object.
  // The object's ACL has to be set to public read.
  // [START public_url]
  function getPublicUrl(filename) {
    return `https://storage.googleapis.com/my-lib-project.appspot.com/${filename}`;
  }
  const Multer = require('multer');
  const multer = Multer({
    storage: Multer.MemoryStorage
  });
  // [END public_url]
  app.post('/upload', multer.single('file'), _sendUploadToGCS)
  function _sendUploadToGCS(req, res, next) {
    // console.log(req.file)
    if (!req.file) {
      return res.json({
        errorMessage: 'Error by Database',
        data: null
      });
    }
    const gcsname = `${new Date().getTime() + req.file.name}`;
    const file = bucket.file(gcsname);
    const stream = file.createWriteStream({
      metadata: {
        contentType: req.file.mimetype
      }
    });
    stream.on('error', (err) => {
      req.file.cloudStorageError = err;
      res.json({
        errorMessage: 'Error by Database',
        data: err
      });
    });
    stream.on('finish', () => {
      req.file.cloudStorageObject = `https://storage.googleapis.com/my-lib-project.appspot.com/${gcsname}`;;
      file.makePublic().then(() => {
        req.file.cloudStoragePublicUrl = `https://storage.googleapis.com/my-lib-project.appspot.com/${gcsname}`;
        res.json({
          successMessage: 'Success',
          data: `https://storage.googleapis.com/my-lib-project.appspot.com/${gcsname}`
        });
      });
    });
    stream.end(req.file.buffer);
  }
  // [END process]

  // Multer handles parsing multipart/form-data requests.
  // This instance is configured to store images in memory.
  // This makes it straightforward to upload to Cloud Storage.
  // [START multer]

};
